import React from 'react';
// import './styles.css';
import styles from './logout.scss';

const handleLogout = () => {
  sessionStorage.clear();
  window.location.reload();
};

const ButtonLogout = () => (
  <div className={`${styles.container}`}>
    <button className= {`${styles['btn-logout']}`} onClick={handleLogout}>Logout</button>
  </div>
)
;

export default ButtonLogout;
